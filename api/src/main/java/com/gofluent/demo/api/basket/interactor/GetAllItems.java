package com.gofluent.demo.api.basket.interactor;

import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.demo.api.basket.response.BasketResource;

import java.util.List;

public interface GetAllItems {
    List<BasketResource> execute(Long userId);
}
