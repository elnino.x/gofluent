package com.gofluent.demo.api.basket.interactor.impl;

import com.gofluent.demo.api.basket.interactor.CreateBasket;
import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.demo.api.basket.response.BasketResource;
import com.gofluent.shopping.basket.gateway.BasketGateway;
import com.gofluent.shopping.basket.model.Basket;
import com.unionbankph.andal.common.mapper.JacksonMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class CreateBasketImpl implements CreateBasket {

    private final BasketGateway basketGateway;
    private final JacksonMapper jacksonMapper;

    @Override
    public BasketResource execute(BasketForm basketForm) {
        Basket basket = basketGateway.addToBasket(basketForm);
        return jacksonMapper.mapTo(basket, BasketResource.class);
    }
}
