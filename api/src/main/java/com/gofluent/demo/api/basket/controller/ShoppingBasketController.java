package com.gofluent.demo.api.basket.controller;

import com.gofluent.demo.api.basket.interactor.CreateBasket;
import com.gofluent.demo.api.basket.interactor.DeleteItems;
import com.gofluent.demo.api.basket.interactor.GetAllItems;
import com.gofluent.demo.api.basket.interactor.GetTotalAmount;
import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.demo.api.basket.response.AmountResource;
import com.gofluent.demo.api.basket.response.BasketResource;
import com.unionbankph.andal.api.common.annotation.SupportedApiVersion;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import retrofit2.http.Body;

import java.util.List;

import static com.gofluent.demo.common.constant.ApiVersion.V1;

@RestController
@SupportedApiVersion(V1)
@RequestMapping("/demo")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@Slf4j
public class ShoppingBasketController {

    private final CreateBasket createBasket;
    private final DeleteItems deleteItems;
    private final GetAllItems getAllItems;
    private final GetTotalAmount getTotalAmount;

    @PostMapping("/create")
    public BasketResource createBasket(@Body BasketForm basketForm){
        return createBasket.execute(basketForm);
    }

    @DeleteMapping("/remove")
    void deleteItems(@RequestParam(value = "id", required = true) Long id) {
        deleteItems.execute(id);
    }

    @GetMapping("/basket")
    public List<BasketResource> getBasket(@RequestParam(value = "userId", required = true) long userId) {
        return getAllItems.execute(userId);
    }

    @GetMapping("/amount")
    public AmountResource getAmount(@RequestParam(value = "userId", required = true) long userId) {
        return getTotalAmount.execute(userId);
    }
}
