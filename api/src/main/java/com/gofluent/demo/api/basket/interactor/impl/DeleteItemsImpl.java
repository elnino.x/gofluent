package com.gofluent.demo.api.basket.interactor.impl;

import com.gofluent.demo.api.basket.interactor.DeleteItems;
import com.gofluent.shopping.basket.gateway.BasketGateway;
import com.gofluent.shopping.basket.model.Basket;
import com.unionbankph.andal.common.exception.ApiException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class DeleteItemsImpl implements DeleteItems {

    private final BasketGateway basketGateway;

    @Override
    public void execute(long id) {
        Basket basket = basketGateway.retrieveByUserId(id);

        if(Objects.isNull(basket)){
            throw new ApiException("No basket for id " + id);
        }
        basketGateway.deleteById(id);
    }
}
