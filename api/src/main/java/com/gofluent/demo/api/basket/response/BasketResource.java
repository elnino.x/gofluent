package com.gofluent.demo.api.basket.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasketResource {
    private Long id;
    private BigDecimal amount;
    private String itemDescription;
    private Long quantity;
    private Long userId;
    private Date createDate;
}
