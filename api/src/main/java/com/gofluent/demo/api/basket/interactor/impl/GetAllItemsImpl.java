package com.gofluent.demo.api.basket.interactor.impl;

import com.gofluent.demo.api.basket.interactor.GetAllItems;
import com.gofluent.demo.api.basket.response.BasketResource;
import com.gofluent.shopping.basket.gateway.BasketGateway;
import com.unionbankph.andal.common.exception.ApiException;
import com.unionbankph.andal.common.mapper.JacksonMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class GetAllItemsImpl implements GetAllItems {

    private final BasketGateway basketGateway;
    private final JacksonMapper jacksonMapper;

    @Override
    public List<BasketResource> execute(Long userId) {
        List<BasketResource> currentBasket = basketGateway.retrieveBasketByUserId(userId)
                .stream()
                .map(basket -> jacksonMapper.mapTo(basket, BasketResource.class))
                .collect(Collectors.toList());

        if (Objects.isNull(currentBasket)) {
            throw new ApiException("No Basket for userId " + userId);
        }

        return currentBasket;
    }
}
