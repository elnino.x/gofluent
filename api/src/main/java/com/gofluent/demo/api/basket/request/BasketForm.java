package com.gofluent.demo.api.basket.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BasketForm {
    private BigDecimal amount;
    private String itemDescription;
    private Long quantity;
    private Long userId;
}
