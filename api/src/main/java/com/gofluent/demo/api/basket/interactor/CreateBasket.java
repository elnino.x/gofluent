package com.gofluent.demo.api.basket.interactor;

import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.demo.api.basket.response.BasketResource;

public interface CreateBasket {
    BasketResource execute(BasketForm basketForm);
}
