package com.gofluent.demo.api.basket.interactor;

import com.gofluent.demo.api.basket.response.AmountResource;

public interface GetTotalAmount {
    AmountResource execute(Long userId);
}
