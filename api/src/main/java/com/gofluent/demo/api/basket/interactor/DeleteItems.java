package com.gofluent.demo.api.basket.interactor;

public interface DeleteItems {
    void execute(long id);
}
