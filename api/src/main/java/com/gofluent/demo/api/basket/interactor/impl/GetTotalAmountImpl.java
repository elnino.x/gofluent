package com.gofluent.demo.api.basket.interactor.impl;

import com.gofluent.demo.api.basket.interactor.GetTotalAmount;
import com.gofluent.demo.api.basket.response.AmountResource;
import com.gofluent.shopping.basket.gateway.BasketGateway;
import com.gofluent.shopping.basket.model.Basket;
import com.unionbankph.andal.common.mapper.JacksonMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class GetTotalAmountImpl implements GetTotalAmount {

    private final BasketGateway basketGateway;
    private final JacksonMapper jacksonMapper;

    @Override
    public AmountResource execute(Long userId) {
        AmountResource amountResource = new AmountResource();
        List<Basket> currentBasket = basketGateway.retrieveBasketByUserId(userId);
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (Basket itemAmount : currentBasket) {
            totalAmount = totalAmount.add(itemAmount.getAmount());
            amountResource.setTotalAmount(totalAmount);
            amountResource.setUserId(userId);
        }
        return amountResource;
    }
}
