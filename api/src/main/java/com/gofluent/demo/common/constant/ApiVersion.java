package com.gofluent.demo.common.constant;

public enum ApiVersion {
    V1("1");

    private String version;

    ApiVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
