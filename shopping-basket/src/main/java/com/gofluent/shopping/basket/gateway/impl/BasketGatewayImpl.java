package com.gofluent.shopping.basket.gateway.impl;

import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.shopping.basket.gateway.BasketGateway;
import com.gofluent.shopping.basket.model.Basket;
import com.gofluent.shopping.basket.persistence.entity.BasketEntity;
import com.gofluent.shopping.basket.persistence.repository.BasketRepository;
import com.unionbankph.andal.common.mapper.JacksonMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@Slf4j
public class BasketGatewayImpl implements BasketGateway {

    private final JacksonMapper jacksonMapper;
    private final BasketRepository basketRepository;

    @Override
    public List<Basket> getAllBasket(Long userId) {
        return null;
    }

    @Override
    public void deleteById(Long id) {
        basketRepository.deleteById(id);
    }

    @Override
    public Basket addToBasket(BasketForm basketForm) {
        return jacksonMapper.mapTo(
                basketRepository.save(jacksonMapper.mapTo(basketForm, BasketEntity.class)), Basket.class);
    }

    @Override
    public Basket retrieveByUserId(Long id) {
        return jacksonMapper.mapTo(basketRepository.findById(id), Basket.class);
    }

    @Override
    public List<Basket> retrieveBasketByUserId(Long userId) {
        return basketRepository.findByUserId(userId)
                .stream()
                .map(basketEntity -> jacksonMapper.mapTo(basketEntity, Basket.class))
                .collect(Collectors.toList());
    }
}
