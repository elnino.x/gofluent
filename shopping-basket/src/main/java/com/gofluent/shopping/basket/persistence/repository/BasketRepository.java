package com.gofluent.shopping.basket.persistence.repository;

import com.gofluent.shopping.basket.persistence.entity.BasketEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BasketRepository extends JpaRepository<BasketEntity, Long> {
    List<BasketEntity> findByUserId(Long userId);

}
