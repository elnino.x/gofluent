package com.gofluent.shopping.basket.gateway;

import com.gofluent.demo.api.basket.request.BasketForm;
import com.gofluent.shopping.basket.model.Basket;

import java.util.List;

public interface BasketGateway {
    List<Basket> getAllBasket(Long userId);
    void deleteById(Long userId);
    Basket addToBasket(BasketForm basketForm);
    Basket retrieveByUserId(Long id);
    List<Basket> retrieveBasketByUserId(Long userId);

}
