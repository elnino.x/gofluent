package com.gofluent.shopping.basket.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Basket {
    private Long id;
    private BigDecimal amount;
    private String itemDescription;
    private Long quantity;
    private Long userId;
}
