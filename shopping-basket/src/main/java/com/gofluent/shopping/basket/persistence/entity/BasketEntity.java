package com.gofluent.shopping.basket.persistence.entity;

import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "Basket")
@Table(name = "BASKET")
@Getter
@Setter
@ToString
public class BasketEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CREATE_DATE", updatable = false)
    private Date createDate;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "ITEM_DESCRIPTION")
    private String description;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name  = "USER_ID")
    private Long userId;
}
